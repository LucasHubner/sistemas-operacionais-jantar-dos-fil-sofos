/* 
 * File:   main.c
 * Author: lucas
 *
 * Created on 17 de Novembro de 2015, 23:20
 */

#include <stdio.h>
#include <stdlib.h>

#include <pthread.h>
#include <unistd.h>

#define FILOSOFOS 5
#define ESQUERDA (filosofo + FILOSOFOS - 1) % FILOSOFOS
#define DIREITA (filosofo + 1) % FILOSOFOS
#define PENSANDO 0
#define COM_FOME 1
#define COMENDO 2

int estado[FILOSOFOS];
int comedores[FILOSOFOS];

pthread_mutex_t mutex;
pthread_mutex_t mux_filosofos[FILOSOFOS];
pthread_t jantar[FILOSOFOS];
/*
 Foward Declaration!
 */
void * filosofo(void * parametro);
void pegar_garfos(int id);
void com_fome(int id);
void comer(int id);
void pensar(int id);
void devolver_garfos(int id);

void escreve_comedores(){
    int i;
    printf("PLACAR DE COMEDORES\n");
    for(i = 0; i< FILOSOFOS; i++){
        printf("\tFILOSOFO %d: %d", i,comedores[i]);
        printf("\n");
    }
    printf("-----------------------------\n");
}

void * filosofo(void * parametro){
    int id = *((int *) parametro);
    sleep(2);
    while(1){
        //printf("Filosofo %i está pensando\n", id);
        pensar(id);
        pegar_garfos(id);
        //printf("Filosofo %i está comendo\n", id);
        comer(id);
        devolver_garfos(id);
        
    }
}

void pegar_garfos(int filosofo){
    pthread_mutex_lock(&mutex);         // ENTRA NA REGIÃO CRITICA! ESSE CARA É VIDA LOKA
    estado[filosofo] = COM_FOME;        //Avisa qeu está com fome.
    com_fome(filosofo);                 //Avisa que quer pegar os garfos
    pthread_mutex_unlock(&mutex);       //Sai da região critica
    comedores[filosofo]++;
    pthread_mutex_lock(&mux_filosofos[filosofo]);  // Bloqueia garfos
}

void devolver_garfos(int filosofo){
    pthread_mutex_lock(&mutex);
    estado[filosofo] = PENSANDO;    // Altera o estado para pensando
    com_fome(ESQUERDA);             // Verifica se os vizinhos estão com fome
    com_fome(DIREITA);
    pthread_mutex_unlock(&mutex);
    
}

void com_fome(int filosofo){
    if(estado[filosofo] == COM_FOME &&
            estado[ESQUERDA] != COMENDO &&
            estado[DIREITA]  != COMENDO){ //VERIFICA SE ELE PODE PEGAR OS GARFOS
        
        estado[filosofo] = COMENDO;
        pthread_mutex_unlock(&mux_filosofos[filosofo]);
    }
}

void pensar(int filosofo){
    
    long int r = (random() % 9 +1); // Gera um tempo aleatório para comer
    sleep(r);
}
void comer(int filosofo){
    long int r = (random() % 9 +1); // Gera um tempo aleatório para comer
    sleep(r);
}
int main(int argc, char** argv) {
    int estado;
    int i;
/*
    Zera os comedores
 */
    for(i = 0; i<FILOSOFOS;i++){
        comedores[i] = 0;
    }
/*
    Inicia mutex's
 */
    printf("Iniciando Mutex's\n");
    pthread_mutex_init(&mutex, NULL);
    for(i=0; i<FILOSOFOS; i++){
        pthread_mutex_init(&mux_filosofos[i],NULL);
        
    }
/*
    Inicia Threads
*/
    printf("Iniciando Filósofos\n");
    for(i = 0; i< FILOSOFOS; i++){
        estado = pthread_create(&(jantar[i]), NULL, filosofo, (void *) &(i));
        if (estado){
            printf("ERRO!\n");
        }
    }
    printf("Filosofos iniciados\n");
    while(1){
        sleep(1);
        escreve_comedores();
    }
    /*
     Limpa a sujeira
     */
    pthread_mutex_destroy( &(mutex) );
    for(i =0; i<FILOSOFOS; i++){
        pthread_mutex_destroy(&(mux_filosofos[i]));
    }
    pthread_exit(NULL);
    
    return (EXIT_SUCCESS);
}

